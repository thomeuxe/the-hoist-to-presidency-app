cordova.define('cordova/plugin_list', function(require, exports, module) {
module.exports = [
    {
        "file": "plugins/cordova-plugin-inappbrowser/www/inappbrowser.js",
        "id": "cordova-plugin-inappbrowser.inappbrowser",
        "pluginId": "cordova-plugin-inappbrowser",
        "clobbers": [
            "cordova.InAppBrowser.open",
            "window.open"
        ]
    },
    {
        "file": "plugins/cordova-plugin-inappbrowser/src/browser/InAppBrowserProxy.js",
        "id": "cordova-plugin-inappbrowser.InAppBrowserProxy",
        "pluginId": "cordova-plugin-inappbrowser",
        "merges": [
            ""
        ]
    },
    {
        "file": "plugins/com.oauthio.plugins.oauthio/dist/oauth.js",
        "id": "com.oauthio.plugins.oauthio.OAuth",
        "pluginId": "com.oauthio.plugins.oauthio",
        "merges": [
            "OAuth"
        ]
    },
    {
        "file": "plugins/twitter-connect-plugin/www/TwitterConnect.js",
        "id": "twitter-connect-plugin.TwitterConnect",
        "pluginId": "twitter-connect-plugin",
        "clobbers": [
            "TwitterConnect"
        ]
    }
];
module.exports.metadata = 
// TOP OF METADATA
{
    "cordova-plugin-inappbrowser": "1.5.0",
    "com.oauthio.plugins.oauthio": "0.2.4",
    "cordova-plugin-whitelist": "1.3.0",
    "twitter-connect-plugin": "0.6.0"
}
// BOTTOM OF METADATA
});