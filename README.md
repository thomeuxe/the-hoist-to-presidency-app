# The Hoist, 2016 Presidency

## Concept
> *Note de l'auteur :* Bon on va le faire en français parce que faut pas déconner quand même

The Hoist vous permet de matérialiser le soutient que vous apportez à votre candidat favoris aux élections américaines de 2016.

Le jeu consiste à hisser le drapeau de votre parti le plus au possible en vous servant des tweets de soutient que vous avez publiés en guise de monnaie.

Cette monnaie vous permettra de monter toujours plus haut votre drapeau dans ce jeu sans fin en achetant des items qui vous permettront de hisser à votre place pendant que vous n'êtes pas devant l'écran ou qui vous donnerons plus de force lorsque vous hisserez manuellement.

Attention ! Si vous ne tweetez pas pendant 24h votre soutient, les items n'auront plus aucun effet !!

---

Les anims ont été optimisées, ça passe plutot bien ;)

---

> **À venir dans la V2.0** - Mise en commun des score sur le web (serveur node déjà installé), ajout d'item sur le marché


## How to build and run the app

Open your terminal at the project root.
Plug your android device and check that ADB is running.

```
cd src
npm run build
cd ../
cordova platform add android
cordova run android --device
```

If you can't build the app, the apk is available for download on this repository