import PIXI from 'pixi.js'

/**
 * @class A Sprite
 * @augments PIXI.DisplayObjectContainer
 * @constructor
 * @param {PIXI.Texture} texture {@link PIXI.Texture}
 * @param frame
 * @return A new Sprite.
 */
var PixiFlag = function (texture) {
  var points = [];
  for (var i = 0; i < 20; i++) {
    points.push(new PIXI.Point(i * 10, 0));
  }
  this.count = Math.random() * 100;
  this.speed = this.initSpeed = 2.5 + Math.random();
  PIXI.Rope.call(this, texture, points);

  this.position.x = window.innerWidth/2;

  this.amplitude = 0;
  this.resistance = 0.98;

  if (window.DeviceMotionEvent) {
    window.addEventListener('devicemotion', function(e) {
      this.updateMovement(e.acceleration.y);
    }.bind(this), false);
  } else {
    document.getElementById("dmEvent").innerHTML = "Not supported."
  }
}

// constructor
PixiFlag.constructor = PixiFlag;
PixiFlag.prototype = Object.create(PIXI.Rope.prototype);

PixiFlag.prototype.updateMovement = function(acceleration) {
  if(acceleration > 0) {
    this.amplitude = (acceleration > this.amplitude ? acceleration : this.amplitude);
    this.speed = (acceleration/2 > this.speed ? acceleration/2 : this.speed);
  }
}

PixiFlag.prototype.updateTransform = function () {
  PIXI.Rope.prototype.updateTransform.call(this);

  this.count += this.speed;
  var points = this.points;

  for (var i = 0; i < points.length; i++) {
    points[i].y = Math.sin((this.count + (i * 10)) * 0.05) * this.amplitude;
  }

  this.amplitude = Math.max(this.amplitude * this.resistance, 2);
  this.speed = Math.max(this.speed * this.resistance, this.initSpeed);

}

module.exports = PixiFlag;