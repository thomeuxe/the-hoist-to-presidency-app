import PIXI from 'pixi.js'
import store from '../../store'

/**
 * @class A Sprite
 * @augments PIXI.DisplayObjectContainer
 * @constructor
 * @param {PIXI.Texture} texture {@link PIXI.Texture}
 * @param frame
 * @return A new Sprite.
 */
var PixiRope = function (texture) {
  var points = [];
  for (var i = 0; i < 23; i++) {
    points.push(new PIXI.Point(0, (i-1) * window.innerHeight / 20));
  }
  this.count = Math.random() * 100;
  this.speed = this.initSpeed = 2.5 + Math.random();
  PIXI.mesh.Rope.call(this, texture, points);

  this.position.x = window.innerWidth/2;

  this.amplitude = 10;
  this.resistance = 0.995;

  if (window.DeviceMotionEvent) {
    window.addEventListener('devicemotion', function(e) {
      this.updateMovement(e.acceleration.y);
    }.bind(this), false);
  } else {
    document.getElementById("dmEvent").innerHTML = "Not supported."
  }
}

// constructor
PixiRope.constructor = PixiRope;
PixiRope.prototype = Object.create(PIXI.mesh.Rope.prototype);

PixiRope.prototype.updateMovement = function(acceleration) {
  let totalAcceleration = Math.min(acceleration + store.state.data.mps, 50);

  if(totalAcceleration > 0) {
    this.amplitude = (totalAcceleration > this.amplitude ? totalAcceleration : this.amplitude);
    this.speed = (totalAcceleration/2 > this.speed ? totalAcceleration/2 : this.speed);
  }
}

PixiRope.prototype.updateTransform = function () {
  PIXI.mesh.Rope.prototype.updateTransform.call(this);

  this.count += this.speed;
  var points = this.points;

  for (var i = 0; i < points.length; i++) {
    points[i].x = Math.sin((this.count + (i * 10)) * 0.05) * this.amplitude;
  }

  this.amplitude = Math.max(this.amplitude * this.resistance, 2);
  this.speed = Math.max(this.speed * this.resistance, this.initSpeed);

}

module.exports = PixiRope;