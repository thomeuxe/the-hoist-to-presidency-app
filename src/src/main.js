import Vue from 'vue'
import VueRouter from 'vue-router'
import Startscreen from './Startscreen'
import PartySelection from './Pages/PartySelection'
import Hoist from './Pages/Hoist'
import Market from './Pages/Market'
import VueSocketio from 'vue-socket.io'
import Background from './components/Background'
import Alert from './components/Alert'

const App = {}

Vue.use(VueRouter)
Vue.use(VueSocketio, 'https://hoist-to-presidency.herokuapp.com/')
// Vue.use(VueSocketio, 'localhost:5000')

export const router = new VueRouter({transitionOnLoad: true})

router.map({
  '/' : {
    component: Startscreen
  },
  '/select-party' : {
    component: PartySelection
  },
  '/hoist': {
    component: Hoist
  },
  '/market': {
    component: Market
  }
})

router.redirect({
  // redirect any not-found route to home
  '*': '/'
})

function startApp() {
  router.start(App, '#app');

  new Vue({
    el: '#persistentComponent',
    components: { Background, Alert }
  })
}

if(typeof cordova !== 'undefined') {
  document.addEventListener("deviceready", function() {
    startApp()
  }, false);
} else {
    startApp()
}