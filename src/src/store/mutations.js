export const STORAGE_KEY = 'thtp-user'

export const state = {
  data: Object.assign({
    totalMeters: 0,
    lastHoistUpdate: false,
    mps: 0,
    strength: 1,
    items: [],
    party: {},
    user: {},
    money: 0,
    tweets: [],
  }, JSON.parse(window.localStorage.getItem(STORAGE_KEY) || '{}'))
}

if(state.data.lastTweet) {
  state.data.lastTweet = new Date(state.data.lastTweet)
} else {
  state.data.lastTweet = new Date(0)
}

state.autoHoistEnabled = false;
state.gameReady = false;

state.alert = {
  tweet_count: 0,
  retweet_count: 0,
  favorite_count: 0,
  money: 0
};

export const mutations = {
  setGameReady: function (state, isReady) {
    if(typeof isReady !== 'undefined') {
      state.gameReady = isReady;
    } else {
      state.gameReady = true;
    }
  },
  addToTotalMeters (state, { amount }) {
    state.data.lastHoistUpdate = new Date().getTime();
    state.data.totalMeters = parseFloat((state.data.totalMeters + amount).toFixed(2));
  },
  updateLastHoistUpdate (state) {
    state.data.lastHoistUpdate = new Date().getTime();
  },
  addItem (state, { item }) {
    let existingItem = state.data.items.find((el) => { return el.id === item.id });

    if (existingItem) {
      existingItem.amount++;
    } else {
      item.amount = 1;
      state.data.items.push(item);
    }

    state.data.mps = state.data.mps + (item.effects.mps || 0);
    state.data.strength = state.data.strength + (item.effects.strength || 0);
  },
  setParty (state, { party }) {
    state.data.party = party;
  },
  setUser (state, { user }) {
    state.data.user = {
      userId: user.userId,
      userName: user.userName,
      name: user.name
    }
  },
  setApiInstance (state, { instance }) {
    state.api = instance;
  },
  addTweet(state, { tweet }) {
    state.data.tweets = state.data.tweets || [];

    if(state.data.lastTweet && !(state.data.lastTweet instanceof Date)) {
      state.data.lastTweet = new Date(state.data.lastTweet);
    }

    if (!state.data.lastTweet || state.data.lastTweet.getTime() < new Date(tweet.created_at)) {
      state.data.lastTweet = new Date(tweet.created_at);
    }

    state.data.tweets.push({
      id: tweet.id,
      value: tweet.value,
      content: tweet.text,
      created_at: tweet.created_at,
      favorite_count: tweet.favorite_count,
      retweet_count: tweet.retweet_count
    });

    state.alert.money += tweet.value;
    state.alert.tweet_count ++;
    state.alert.retweet_count += tweet.retweet_count;
    state.alert.favorite_count += tweet.favorite_count;
  },
  updateTweet(state, { id, updates }) {
    var updatedTweet = state.data.tweets.find((el) => { return el.id === id });

    if(typeof updatedTweet !== "undefined") {
      Object.assign(updatedTweet, {
        id: updates.id,
        value: updates.value,
        content: updates.text,
        created_at: updates.created_at,
        favorite_count: updates.favorite_count,
        retweet_count: updates.retweet_count
      });

      state.alert.money += updates.value - updatedTweet.value;
      state.alert.retweet_count += updates.retweet_count - updatedTweet.retweet_count;
      state.alert.favorite_count += updates.favorite_count - updatedTweet.favorite_count;
    }
  },
  resetAlert (state) {
    state.alert.money = 0;
    state.alert.tweet_count = 0;
    state.alert.retweet_count = 0;
    state.alert.favorite_count = 0;
  },
  updateMoney (state, { value }) {
    state.data.money += value;
  },
  toggleAutoHoist (state, { enable }) {
    state.autoHoistEnabled = enable;
  }
}