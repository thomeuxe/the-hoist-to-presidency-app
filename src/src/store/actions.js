import store from './'

export default {
  getTweets: function (callback) {
    callback = callback || function () {};

    if (store.state.api !== undefined) {
      store.state.api.get('1.1/search/tweets.json?count=1000&q=' + store.state.data.party.tweetSearch + '+from:' + store.state.data.user.userName).done((tweets) => {

        this.computeTweets({
          tweets: tweets.statuses
        })

        callback(tweets.statuses)

      }).fail((err) => {
        console.warn('Cannot get tweets from ' + username);

        callback()
      });
    } else {
      let tweets = [
        {
          id: 1,
          favorite_count: 24,
          retweet_count: 3,
          text: "#1 This is a test tweet",
          created_at: "Thu Sep 21 17:21:07 +0000 2016"
        },
        {
          id: 12,
          favorite_count: 8,
          retweet_count: 3,
          text: "#2 This is a test tweet",
          created_at: "Thu Sep 29 20:30:07 +0000 2016"
        },
        {
          id: 4,
          favorite_count: 8,
          retweet_count: 3,
          text: "#3 This is a test tweet",
          created_at: "Sat Oct 01 10:12:07 +0000 2016"
        },
        {
          id: 401,
          favorite_count: 8,
          retweet_count: 3,
          text: "#3 This is a test tweet",
          created_at: "Sat Oct 01 10:12:07 +0000 2016"
        }
      ];

      this.computeTweets({ tweets });

      callback(tweets);
    }
  },
  computeTweets({ tweets }) {
    store.state.data.tweets = store.state.data.tweets || [];

    tweets.forEach((tweet) => {


      var dbTweet = store.state.data.tweets.find((el) => { return el.id === tweet.id });

      let value = 1 + tweet.favorite_count * 2 + tweet.retweet_count * 5;

      var tweetData = {
        id: tweet.id,
        value: value,
        text: tweet.text,
        created_at: tweet.created_at,
        favorite_count: tweet.favorite_count,
        retweet_count: tweet.retweet_count
      };

      if (typeof dbTweet === "undefined") {

        store.dispatch('addTweet', {
          tweet: tweetData
        });

        store.dispatch('updateMoney', { value: value });
      } else {

        store.dispatch('updateTweet', {
          id: tweet.id,
          updates: tweetData
        });

        store.dispatch('updateMoney', { value: value - dbTweet.value });
      }
    });

    store.state.data.tweets.sort((a, b) => {
      return (a.id < b.id) ? 1 : -1;
    });
  },
  initTweetTimeoutFetch: function () {
    if (!this.lastTweetFetch) {
      this.lastTweetFetch = new Date(0);
      this.tweetRaq = requestAnimationFrame(this.tweetTimeoutFetch.bind(this));
    }
  },
  tweetTimeoutFetch: function () {
    this.tweetRaq = requestAnimationFrame(this.tweetTimeoutFetch.bind(this));

    if (new Date().getTime() - this.lastTweetFetch.getTime() > 20000) {
      this.lastTweetFetch = new Date();
      this.getTweets();
    }
  },
  buyItem({ item, value }) {
    if (store.state.data.money >= value) {
      store.dispatch('addItem', { item });
      store.dispatch('updateMoney', { value: -value });
      return true;
    } else {
      return false;
    }
  }
};