import { STORAGE_KEY } from './mutations'

let lastSave = false;

const localStoragePlugin = store => {
  store.subscribe((mutation, { data }) => {
    if(!lastSave || new Date().getTime() - lastSave > 2000) {
      window.localStorage.setItem(STORAGE_KEY, JSON.stringify(data));
      lastSave = new Date().getTime();
    }
  })
}

export default [localStoragePlugin]